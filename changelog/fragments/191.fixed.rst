The option ``mutation`` of the Differential Evolution algorithm now checks the correct expected type.
