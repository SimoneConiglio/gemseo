#
# This file is autogenerated by pip-compile with python 3.8
# To update, run:
#
#    pip-compile requirements/doc.in
#
alabaster==0.7.12
    # via sphinx
autodocsumm==0.2.7
    # via -r requirements/doc.in
babel==2.9.1
    # via sphinx
certifi==2021.5.30
    # via requests
charset-normalizer==2.0.6
    # via requests
custom-inherit @ git+https://github.com/AntoineD/custom_inherit.git@master
    # via -r requirements/doc.in
docutils==0.17.1
    # via
    #   pybtex-docutils
    #   sphinx
idna==3.2
    # via requests
imagesize==1.2.0
    # via sphinx
jinja2==3.0.1
    # via sphinx
latexcodec==2.0.1
    # via pybtex
markupsafe==2.0.1
    # via jinja2
oset==0.1.3
    # via sphinxcontrib.bibtex
packaging==20.9
    # via
    #   -c requirements/test-python3.txt
    #   sphinx
pbr==5.6.0
    # via sphinxcontrib.apidoc
pillow==8.3.2
    # via -r requirements/doc.in
pybtex==0.24.0
    # via
    #   pybtex-docutils
    #   sphinxcontrib.bibtex
pybtex-docutils==1.0.1
    # via sphinxcontrib.bibtex
pyenchant==3.2.1
    # via
    #   -r requirements/doc.in
    #   sphinxcontrib.spelling
pygments==2.10.0
    # via sphinx
pyparsing==2.4.7
    # via
    #   -c requirements/test-python3.txt
    #   packaging
pytz==2021.1
    # via babel
pyyaml==5.4.1
    # via pybtex
requests==2.26.0
    # via sphinx
six==1.16.0
    # via
    #   latexcodec
    #   pybtex
snowballstemmer==2.1.0
    # via sphinx
sphinx==4.2.0
    # via
    #   -r requirements/doc.in
    #   autodocsumm
    #   sphinx-gallery
    #   sphinxcontrib.apidoc
    #   sphinxcontrib.bibtex
    #   sphinxcontrib.plantuml
    #   sphinxcontrib.spelling
sphinx-gallery==0.9.0
    # via -r requirements/doc.in
sphinxcontrib-applehelp==1.0.2
    # via sphinx
sphinxcontrib-devhelp==1.0.2
    # via sphinx
sphinxcontrib-htmlhelp==2.0.0
    # via sphinx
sphinxcontrib-jsmath==1.0.1
    # via sphinx
sphinxcontrib-qthelp==1.0.3
    # via sphinx
sphinxcontrib-serializinghtml==1.1.5
    # via sphinx
sphinxcontrib.apidoc==0.3.0
    # via -r requirements/doc.in
sphinxcontrib.bibtex==1.0.0
    # via -r requirements/doc.in
sphinxcontrib.plantuml==0.21
    # via -r requirements/doc.in
sphinxcontrib.spelling==7.2.1
    # via -r requirements/doc.in
urllib3==1.26.6
    # via requests

# The following packages are considered to be unsafe in a requirements file:
# setuptools
